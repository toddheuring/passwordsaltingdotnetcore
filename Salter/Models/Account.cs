using Newtonsoft.Json;

namespace Salter.Models
{
    public class Account
    {
        public int? Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Account(int id, string email, string password, string firstName, string lastName)
        {
            Id = id;
            Email = email;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
        }

        [JsonConstructor]
        public Account(string email, string password, string firstName, string lastName)
        {
            Id = null;
            Email = email;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
        }
    }
}