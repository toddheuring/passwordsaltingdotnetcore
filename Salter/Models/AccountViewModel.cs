namespace Salter.Models
{
    public class AccountViewModel
    {
        public int? Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public AccountViewModel(Account account)
        {
            Id = account.Id;
            Email = account.Email;
            FirstName = account.FirstName;
            LastName = account.LastName;
        }
    }
}