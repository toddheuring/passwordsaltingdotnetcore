using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Salter.Models;
using Salter.Repositories;

namespace Salter.Services
{
    public interface IAccountService
    {
        Account Authenticate(Login login);
        Account CreateAccount(Account account);
    }

    public class AccountService : IAccountService
    {
        private readonly IAccountRepository _accountRepository;

        public AccountService(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public Account Authenticate(Login login)
        {
            var account = _accountRepository.GetAccountByEmail(login.Email);

            var parts = account.Password.Split(':');

            var salt = Convert.FromBase64String(parts[0]);

            return HashPassword(salt, login.Password).Equals(parts[1]) ? account : null;
        }

        public Account CreateAccount(Account account)
        {
            // generate a 128-bit salt using a secure PRNG
            var salt = new byte[128];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }

            // generate hashed password
            var hashed = HashPassword(salt, account.Password);

            // combine salt and hashed password
            var password = $"{Convert.ToBase64String(salt)}:{hashed}";

            account = new Account(account.Email, password, account.FirstName, account.LastName);

            return _accountRepository.CreateAccount(account);
        }

        private static string HashPassword(byte[] salt, string password)
        {
            var hashed = Convert.ToBase64String(
                KeyDerivation.Pbkdf2(
                    password: password,
                    salt: salt,
                    prf: KeyDerivationPrf.HMACSHA1,
                    iterationCount: 10000,
                    numBytesRequested: 512));

            return hashed;
        }
    }
}
