﻿using Microsoft.AspNetCore.Mvc;
using Salter.Models;
using Salter.Services;

namespace Salter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService) {
            _accountService = accountService;
        }
        
        [Route("create")]
        [HttpPost]
        public ActionResult<Account> CreateAccount([FromBody] Account account)
        {
            return Ok(new AccountViewModel(_accountService.CreateAccount(account)));
        }
        
        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody] Login login)
        {
            IActionResult response = Unauthorized();
            var account = _accountService.Authenticate(login);
            var accountView = new AccountViewModel(account);

            return account == null ? response : Ok(new {status = "success", account = accountView});
        }
    }
}
