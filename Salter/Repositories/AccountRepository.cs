using Microsoft.Extensions.Configuration;
using Dapper;
using Salter.Models;

namespace Salter.Repositories
{
    public interface IAccountRepository
    {
        Account CreateAccount(Account account);
        Account GetAccountByEmail(string email);
    }

    public class AccountRepository : Repository, IAccountRepository
    {
        public AccountRepository(IConfiguration config) : base(config)
        {
        }

        public Account CreateAccount(Account account)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.QuerySingle<Account>(
                    @"INSERT INTO Account (
                        Email, 
                        Password, 
                        FirstName, 
                        LastName
                    ) 
                    OUTPUT 
                        INSERTED.Id, 
                        INSERTED.Email, 
                        INSERTED.Password, 
                        INSERTED.FirstName, 
                        INSERTED.LastName
                    Values (
                        @Email, 
                        @Password, 
                        @FirstName, 
                        @LastName
                    )",
                    account
                );
            }
        }

        public Account GetAccountByEmail(string email)
        {
            using (var dbConnection = Connection)
            {
                dbConnection.Open();
                return dbConnection.QuerySingle<Account>(
                    @"SELECT 
                        Id,
                        Email, 
                        Password, 
                        FirstName, 
                        LastName 
                    FROM Account 
                    WHERE Email = @email", 
                    new { email }
                );
            }
        }
    }
}