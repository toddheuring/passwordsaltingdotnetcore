using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace Salter.Repositories
{
    public class Repository
    {
        private readonly string _connectionString;

        protected Repository(IConfiguration config)
        {
            var server = config["Database:Server"];
            var database = config["Database:Database"];
            var user = config["Database:User"];
            var password = config["Database:Password"];

            _connectionString = $"Server={server};Database={database};User={user};Password={password}";
        }

        protected IDbConnection Connection => new SqlConnection(_connectionString);
    }
}