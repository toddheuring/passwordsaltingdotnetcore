using NUnit.Framework;
using System.Net;
using System.Net.Http;
using System;
using System.Text;
using Newtonsoft.Json;
using Salter.Models;

namespace Salter.Test
{
    public class AccountTest
    {
        private HttpClient _client;

        [SetUp]
        public void Setup()
        {
            _client = new HttpClient { BaseAddress = new Uri("http://localhost:5000/api/") };
        }

        [Test]
        public void CreateAccount()
        {
            var account = new Account("email", "password", "first", "last");
            var content = new StringContent(account.ToString(), Encoding.UTF8, "application/json");

            var response = _client.PostAsync("accounts/create", content).Result;

            account.Id = 1;
            var expectedJson = JsonConvert.SerializeObject(account);

            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            Assert.AreEqual(expectedJson, response.Content.ReadAsStringAsync().Result);
        }
    }
}